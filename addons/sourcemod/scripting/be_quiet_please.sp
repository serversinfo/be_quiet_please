#pragma semicolon 1
#include <voiceannounce_ex>
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <warden>
#include <basecomm>
#include <csgo_colors>
#include <hlstatsX_adv>
#pragma newdecls required

#define VERSION "1.0.18"

#define DEBUG
#if defined DEBUG
stock void debugMessage(const char[] message, any ...) {
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_be_quiet.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)
#else
#define dbgMsg(%0)
#endif

Handle h_Timer_Round;
Handle h_DeathMessage[MAXPLAYERS];		// для сообщений админам после смерти
bool g_bRoundstart;
bool g_bAllMuted;
bool g_bSpeacking[MAXPLAYERS+1]		= false;
bool g_bAdmSpeacking[MAXPLAYERS+1]	= false;
bool g_bWarden_speacking			= false;
int g_iWarden;

public Plugin myinfo = {
	name		= "[JAIL] - Будь тише, пожалуйста!",
	author		= "Fastmancz & ShaRen",
	description = "Не дает разговаривать заключенным когда говорит КМД",
	version		= VERSION,
	url			= "cmgportal.cz"
}

public void OnPluginStart()
{
	LoadTranslations("be_quiet_please.phrases");
	HookEvent("round_start",		OnRoundStart);
	HookEvent("player_spawn",		OnPlayerSpawn);
	HookEvent("player_death",		OnPlayerDeath);
	HookEvent("player_disconnect",	OnPlayerDeath);
	//dbgMsg("OnPluginStart be_quiet -----------------------------------");
}

public void OnRoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	g_bRoundstart = true;
	g_bWarden_speacking = false;
	if (h_Timer_Round != null && KillTimer(h_Timer_Round))
		h_Timer_Round = null;
	h_Timer_Round = CreateTimer(60.0, Timer_Round);
	for(int i=1; i<MAXPLAYERS; i++)
		if (IsPlayerValid(i) && h_DeathMessage[i] && KillTimer(h_DeathMessage[i]))
			h_DeathMessage[i] = null;
}

public Action Timer_Round(Handle timer)
{
	h_Timer_Round = null;
	g_bRoundstart = false;									// по истечении n сек с начала раунда
	for(int i=1; i<MAXPLAYERS; i++)
		if (IsPlayerValid(i) && warden_iswarden(i))
			CGOPrintToChat(i, "{BLUE}Чтобы Т не перебивали вас во время разговора жмите {RED}E+<кнопка голоса>{BLUE} одновременно.");
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if (IsPlayerValid(client))
		if (IsPlayerAlive(client) && warden_iswarden(client)) {
			if (buttons & IN_USE) {
				if (!g_bAllMuted)
					g_bAllMuted = true;
			} else if(g_bAllMuted)
				g_bAllMuted = false;
		} else if (!IsPlayerAlive(client) && GetAdminFlag(GetUserAdmin(client), Admin_Changemap)) {
			if (buttons & IN_USE && !g_bAdmSpeacking[client] ) {
				g_bAdmSpeacking[client] = true;
				SetClientListeningFlags(client, VOICE_SPEAKALL);
				PrintToChat(client, "Тебя слышат живые");
			} else if (!(buttons & IN_USE ) && g_bAdmSpeacking[client]) {
				g_bAdmSpeacking[client] = false;
				SetClientListeningFlags(client, VOICE_NORMAL);
				PrintToChat(client, "Тебя не слышат живые");
			}
		}
}

public bool OnClientSpeakingEx(int client)
{
	if (IsPlayerValid(client)) {
		if (BaseComm_IsClientMuted(client) && !g_bSpeacking[client]) {		//если у игрока обычный мут
			g_bSpeacking[client] = true;
			PrintCenterText(client, "%t", "notification2");
		} else if ( warden_iswarden(client) && !g_bWarden_speacking && (g_bRoundstart || g_bAllMuted)) {	//если начал говорить КМД
			//PrintToChatAll("%N КМД мутит %b %b", client, g_bRoundstart, g_bAllMuted);
			g_bWarden_speacking = true;
			g_iWarden = client;		// тоже на всякий, если к примеру начал говорить был КМД, закончил говорить уже не КМД
			SetClientListeningFlags(client, VOICE_NORMAL);			// (на всякий) убираем мут если вдруг был
			for(int i=1; i<=MaxClients; i++)
				if (IsPlayerValid(i) && GetClientTeam(i) == 2 && IsPlayerAlive(i)) {
					if (g_bSpeacking[i]) {
						CGOPrintToChat(g_iWarden, "{RED}[jail-мут] {GREEN}%N был заглушен т.к. вы(кмд) начали говорить", i);
						ClientCommand(g_iWarden, "play ui/buttonrollover.wav");
						PrintCenterText(i, "%t", "notification");
					}
					SetClientListeningFlags(i, VOICE_MUTED);
				}
		} else if (GetClientListeningFlags(client) == VOICE_MUTED && !g_bSpeacking[client]) { //Если кто-то с мутом(от КМД) начал говорить 
			g_bSpeacking[client] = true;
			if (!g_bWarden_speacking || !IsPlayerValid(g_iWarden))
				SetClientListeningFlags(client, VOICE_NORMAL);
			else if (g_iWarden) {
				CGOPrintToChat(g_iWarden, "{RED}[jail-мут] {GREEN}%N хотел что-то сказать пока говорит КМД", client);
				ClientCommand(g_iWarden, "play ui/buttonrollover.wav");
				PrintCenterText(client, "%t", "notification");
			}
		} else if (!g_bWarden_speacking && !g_bSpeacking[client]) {
			if (!IsPlayerAlive(client) && GetClientPlayedTime(client) < 3600)
				PrintCenterText(client, "Вас не слышат живые игроки, но слышат мертвые");
			g_bSpeacking[client] = true;
			SetVoiceNormal(client);
		} else if (!g_bSpeacking[client])			// (на всякий случай) если у игрока было g_bSpeacking == false то возвращает значение true
			g_bSpeacking[client] = true;
	} else if (!g_bSpeacking[client])			// (на всякий случай) если у игрока было g_bSpeacking == false то возвращает значение true
		g_bSpeacking[client] = true;
}

// When client stops talk
public int OnClientSpeakingEnd(int client)
{
	g_bSpeacking[client] = false;
	if (warden_iswarden(client)) {
		g_bWarden_speacking = false;
		for (int i = 1; i <= MaxClients; i++)
			SetVoiceNormal(i);
	}
}

void SetVoiceNormal(int client)
{
	if(IsPlayerValid(client)&& !BaseComm_IsClientMuted(client))
		if (GetClientListeningFlags(client) == VOICE_MUTED)
			SetClientListeningFlags(client, VOICE_NORMAL);
}

public void OnPlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	SetVoiceNormal(client);
	if (IsPlayerValid(client) && (warden_iswarden(client) || g_iWarden == client) ) {
		g_bWarden_speacking = false;
		for (int i=1; i<=MaxClients; i++)
			SetVoiceNormal(i);
	}
}

public void OnPlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(IsPlayerValid(client)) {
		if(!BaseComm_IsClientMuted(client))
			SetClientListeningFlags(client, VOICE_NORMAL);		// VOICE_LISTENALL        4    /**< Allow the client to listen to everyone. VOICE_NORMAL не слышат спеков
		if (GetAdminFlag(GetUserAdmin(client), Admin_Changemap))
			h_DeathMessage[client] = CreateTimer(4.0, DeathMessage, client);
	}
}

public Action DeathMessage(Handle timer, any client)
{
	h_DeathMessage[client] = null;
	if (IsPlayerValid(client))
		CGOPrintToChat(client, "{BLUE}Если нужно сказать что-то срочное живым игрокам жмите {RED}E+<кнопка голоса>{BLUE} одновременно.");
}

stock bool IsPlayerValid(int client)
{
	return (client && IsClientInGame(client)) ? true:false;
}